package com.capgemini.jstk.housingdeveloper.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({CustomerServiceTest.class, BuildingServiceTest.class, ApartmentServiceTest.class })
public class ServiceTestSuite {

}