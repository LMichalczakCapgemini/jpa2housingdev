package com.capgemini.jstk.housingdeveloper.service;

import com.capgemini.jstk.housingdeveloper.commons.ApartmentStatus;
import com.capgemini.jstk.housingdeveloper.repository.ApartmentRepository;
import com.capgemini.jstk.housingdeveloper.repository.BuildingRepository;
import com.capgemini.jstk.housingdeveloper.repository.CustomerRepository;
import com.capgemini.jstk.housingdeveloper.entity.ApartmentEnt;
import com.capgemini.jstk.housingdeveloper.entity.BuildingEnt;
import com.capgemini.jstk.housingdeveloper.entity.CustomerEnt;
import com.capgemini.jstk.housingdeveloper.mappers.ApartmentMapper;
import com.capgemini.jstk.housingdeveloper.mappers.BuildingMapper;
import com.capgemini.jstk.housingdeveloper.mappers.CustomerMapper;
import com.capgemini.jstk.housingdeveloper.types.ApartmentTO;
import com.capgemini.jstk.housingdeveloper.types.BuildingTO;
import com.capgemini.jstk.housingdeveloper.types.CustomerTO;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;

public abstract class DataBaseInserts {
    @Autowired
    protected BuildingRepository buildingRepository;

    @Autowired
    protected CustomerRepository customerRepository;

    @Autowired
    protected ApartmentRepository apartmentRepository;

    protected List<ApartmentTO> buildApartments(){
        ApartmentTO apartmentTO1 = new ApartmentTO.ApartmentBuilder().withAddress("Agrestowa1").withStatus(ApartmentStatus.SOLD).withSizeM2(100).withNumberOfRooms(3).withNumberOfBalconies(1).build();
        ApartmentTO apartmentTO2 = new ApartmentTO.ApartmentBuilder().withAddress("Agrestowa2").withStatus(ApartmentStatus.RESERVED).withSizeM2(100).withNumberOfRooms(3).withNumberOfBalconies(1).build();
        ApartmentTO apartmentTO3 = new ApartmentTO.ApartmentBuilder().withAddress("Agrestowa3").withStatus(ApartmentStatus.RESERVED).build();
        ApartmentTO apartmentTO4 = new ApartmentTO.ApartmentBuilder().withAddress("Agrestowa4").withStatus(ApartmentStatus.RESERVED).build();
        ApartmentTO apartmentTO5 = new ApartmentTO.ApartmentBuilder().withAddress("Agrestowa5").withStatus(ApartmentStatus.RESERVED).withFloorNumber(1).build();
        ApartmentTO apartmentTO6 = new ApartmentTO.ApartmentBuilder().withAddress("Agrestowa6").withStatus(ApartmentStatus.RESERVED).withFloorNumber(1).build();
        ApartmentTO apartmentTO7 = new ApartmentTO.ApartmentBuilder().withAddress("Agrestowa7").withStatus(ApartmentStatus.FREE).withFloorNumber(1).build();
        ApartmentTO apartmentTO8 = new ApartmentTO.ApartmentBuilder().withAddress("Agrestowa8").withStatus(ApartmentStatus.FREE).withFloorNumber(1).build();
        ApartmentTO apartmentTO9 = new ApartmentTO.ApartmentBuilder().withAddress("Bajana1").withStatus(ApartmentStatus.SOLD).build();
        ApartmentTO apartmentTO10 = new ApartmentTO.ApartmentBuilder().withAddress("Bajana2").withStatus(ApartmentStatus.RESERVED).withSizeM2(100).withNumberOfRooms(3).withNumberOfBalconies(1).build();
        ApartmentTO apartmentTO11 = new ApartmentTO.ApartmentBuilder().withAddress("Bajana3").withStatus(ApartmentStatus.RESERVED).build();
        ApartmentTO apartmentTO12 = new ApartmentTO.ApartmentBuilder().withAddress("Bajana4").withStatus(ApartmentStatus.RESERVED).withFloorNumber(1).build();
        ApartmentTO apartmentTO13 = new ApartmentTO.ApartmentBuilder().withAddress("Bajana5").withStatus(ApartmentStatus.FREE).withFloorNumber(1).build();
        ApartmentTO apartmentTO14 = new ApartmentTO.ApartmentBuilder().withAddress("Bajana6").withStatus(ApartmentStatus.FREE).withFloorNumber(1).build();
        ApartmentTO apartmentTO15 = new ApartmentTO.ApartmentBuilder().withAddress("Cyfrowa1").withStatus(ApartmentStatus.SOLD).withPricePln(10).build();
        ApartmentTO apartmentTO16 = new ApartmentTO.ApartmentBuilder().withAddress("Cyfrowa2").withStatus(ApartmentStatus.SOLD).withPricePln(20).build();
        ApartmentTO apartmentTO17 = new ApartmentTO.ApartmentBuilder().withAddress("Cyfrowa3").withStatus(ApartmentStatus.SOLD).withFloorNumber(1).withPricePln(30).build();
        ApartmentTO apartmentTO18 = new ApartmentTO.ApartmentBuilder().withAddress("Cyfrowa4").withStatus(ApartmentStatus.RESERVED).withFloorNumber(1).withPricePln(40).build();
        ApartmentTO apartmentTO19 = new ApartmentTO.ApartmentBuilder().withAddress("Cyfrowa5").withStatus(ApartmentStatus.FREE).withFloorNumber(1).withPricePln(50).build();

        return Lists.newArrayList(apartmentTO1,apartmentTO2,apartmentTO3,apartmentTO4,apartmentTO5,apartmentTO6, apartmentTO7, apartmentTO8,
                apartmentTO9,apartmentTO10,apartmentTO11,apartmentTO12,apartmentTO13, apartmentTO14, apartmentTO15,
                apartmentTO16,apartmentTO17,apartmentTO18,apartmentTO19);
    }

    protected List<BuildingEnt> buildAndSaveBuildings(List<ApartmentTO> apartments){
        BuildingTO buildingTO1 = new BuildingTO.BuildingBuilder().withApartments(new HashSet<>(apartments.subList(0,8)))
                .withLocalization("Wrocław").withIsElevator(true).build();
        BuildingTO buildingTO2 = new BuildingTO.BuildingBuilder().withApartments(new HashSet<>(apartments.subList(8,14)))
                .withLocalization("Kraków").build();
        BuildingTO buildingTO3 = new BuildingTO.BuildingBuilder().withApartments(new HashSet<>(apartments.subList(14,19)))
                .withLocalization("Warszawa").build();

        BuildingEnt buildingEnt1 = buildingRepository.save(BuildingMapper.INSTANCE.toBuildingEnt(buildingTO1));
        BuildingEnt buildingEnt2 = buildingRepository.save(BuildingMapper.INSTANCE.toBuildingEnt(buildingTO2));
        BuildingEnt buildingEnt3 = buildingRepository.save(BuildingMapper.INSTANCE.toBuildingEnt(buildingTO3));

        return Lists.newArrayList(buildingEnt1, buildingEnt2, buildingEnt3);
    }

    protected List<CustomerEnt> buildAndSaveCustomers(List<ApartmentEnt> apartments){
        CustomerTO customerTO1 = new CustomerTO.CustomerBuilder()
                .withApartments(ApartmentMapper.INSTANCE.map2TOs(new HashSet<>(apartments.subList(0,2))))
                .withFirstName("A").withLastName("AA").withEmail("Am").build();
        CustomerTO customerTO2 = new CustomerTO.CustomerBuilder()
                .withApartments(ApartmentMapper.INSTANCE.map2TOs(new HashSet<>(apartments.subList(1,3))))
                .withFirstName("B").withLastName("BB").withEmail("Bm").build();
        CustomerTO customerTO3 = new CustomerTO.CustomerBuilder()
                .withApartments(ApartmentMapper.INSTANCE.map2TOs(new HashSet<>(apartments.subList(3,6))))
                .withFirstName("C").withLastName("CC").withEmail("Cm").build();
        CustomerTO customerTO4 = new CustomerTO.CustomerBuilder()
                .withApartments(ApartmentMapper.INSTANCE.map2TOs(new HashSet<>(apartments.subList(8,9))))
                .withFirstName("D").withLastName("DD").withEmail("Dm").build();
        CustomerTO customerTO5 = new CustomerTO.CustomerBuilder()
                .withApartments(ApartmentMapper.INSTANCE.map2TOs(new HashSet<>(apartments.subList(9,12))))
                .withFirstName("E").withLastName("EE").withEmail("Em").build();
        CustomerTO customerTO6 = new CustomerTO.CustomerBuilder()
                .withFirstName("F").withLastName("FF").withEmail("Fm").build();
        CustomerTO customerTO7 = new CustomerTO.CustomerBuilder()
                .withApartments(ApartmentMapper.INSTANCE.map2TOs(new HashSet<>(apartments.subList(14,17))))
                .withFirstName("G").withLastName("GG").withEmail("Gm").build();
        CustomerTO customerTO8 = new CustomerTO.CustomerBuilder()
                .withApartments(ApartmentMapper.INSTANCE.map2TOs(new HashSet<>(apartments.subList(14,17))))
                .withFirstName("H").withLastName("HH").withEmail("Hm").build();
        CustomerTO customerTO9 = new CustomerTO.CustomerBuilder()
                .withApartments(ApartmentMapper.INSTANCE.map2TOs(new HashSet<>(apartments.subList(14,17))))
                .withFirstName("I").withLastName("II").withEmail("Im").build();
        CustomerTO customerTO10 = new CustomerTO.CustomerBuilder()
                .withApartment(ApartmentMapper.INSTANCE.toApartmentTO(apartments.get(17)))
                .withFirstName("J").withLastName("JJ").withEmail("Jm").build();
        CustomerTO customerTO11 = new CustomerTO.CustomerBuilder()
                .withApartment(ApartmentMapper.INSTANCE.toApartmentTO(apartments.get(17)))
                .withFirstName("K").withLastName("KK").withEmail("Km").build();
        CustomerTO customerTO12 = new CustomerTO.CustomerBuilder()
                .withApartment(ApartmentMapper.INSTANCE.toApartmentTO(apartments.get(17)))
                .withApartment(ApartmentMapper.INSTANCE.toApartmentTO(apartments.get(1)))
                .withFirstName("L").withLastName("LL").withEmail("Lm").build();

        CustomerEnt customerEnt1 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO1));
        CustomerEnt customerEnt2 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO2));
        CustomerEnt customerEnt3 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO3));
        CustomerEnt customerEnt4 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO4));
        CustomerEnt customerEnt5 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO5));
        CustomerEnt customerEnt6 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO6));
        CustomerEnt customerEnt7 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO7));
        CustomerEnt customerEnt8 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO8));
        CustomerEnt customerEnt9 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO9));
        CustomerEnt customerEnt10 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO10));
        CustomerEnt customerEnt11 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO11));
        CustomerEnt customerEnt12 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO12));

        return Lists.newArrayList(customerEnt1, customerEnt2,customerEnt3, customerEnt4,customerEnt5,
                customerEnt6, customerEnt7, customerEnt8,customerEnt9, customerEnt10, customerEnt11, customerEnt12);
    }
}
