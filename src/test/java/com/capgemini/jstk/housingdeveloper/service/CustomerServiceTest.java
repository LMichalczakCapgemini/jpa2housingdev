package com.capgemini.jstk.housingdeveloper.service;

import com.capgemini.jstk.housingdeveloper.commons.*;
import com.capgemini.jstk.housingdeveloper.entity.ApartmentEnt;
import com.capgemini.jstk.housingdeveloper.entity.CustomerEnt;
import com.capgemini.jstk.housingdeveloper.mappers.CustomerMapper;
import com.capgemini.jstk.housingdeveloper.types.CustomerTO;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class CustomerServiceTest extends DataBaseInserts {

    @Autowired
    private CustomerService sut;

    private List<CustomerEnt> savedCustomers;
    private List<ApartmentEnt> savedApartments;

    @Before
    public void insertToDataBase() {
        buildAndSaveBuildings(buildApartments());
        Iterable<ApartmentEnt> savedApartmentsIterable = apartmentRepository.findAll();
        savedApartments = Lists.newArrayList(savedApartmentsIterable);
        savedApartments.sort(Comparator.comparing(ApartmentEnt::getAddress));
        savedCustomers = buildAndSaveCustomers(savedApartments);
    }

    @Test
    @Transactional
    public void findCustomersThatBuyOrReserveSpecificApartment(){
        List<CustomerTO> foundCustomers = sut.findCustomersByApartment(savedApartments.get(1).getId());

        assertThat(foundCustomers.size(), is(equalTo(3)));
    }

    @Test
    @Transactional
    public void shouldReserveApartmentByCustomer(){
        Long reservedApartmentId = savedApartments.get(3).getId();

        CustomerTO updatedCustomer = sut.reserveApartment(reservedApartmentId, savedCustomers.get(0).getId());

        assertThat(updatedCustomer.getApartments().size(), is(equalTo(3)));
        assertThat(apartmentRepository.findById(reservedApartmentId).get().getStatus(), is(equalTo(ApartmentStatus.RESERVED)));
    }

    @Test(expected = CustomerIsAlreadyOwnerOfThatApartmentException.class)
    @Transactional
    public void shouldExceptionWhenReserveAlreadyReservedByCustomerApartment(){
        Long newOwnerId = savedCustomers.get(0).getId();
        Long apartmentToReserveId = savedApartments.get(1).getId();

        sut.reserveApartment(apartmentToReserveId,newOwnerId);
    }

    @Test(expected = ApartmentIsSoldException.class)
    @Transactional
    public void shouldExceptionWhenReserveAlreadySoldApartment(){
        Long newOwnerId = savedCustomers.get(1).getId();
        Long apartmentToReserveId = savedApartments.get(0).getId();

        sut.reserveApartment(apartmentToReserveId,newOwnerId);
    }

    @Test(expected = CustomerMadeMaxNumberOfOnlyOwningReservationsException.class)
    @Transactional
    public void shouldExceptionWhenHas3AloneReservation(){
        Long newOwnerId = savedCustomers.get(4).getId();
        Long apartmentToReserveId = savedApartments.get(18).getId();

        sut.reserveApartment(apartmentToReserveId,newOwnerId);
    }

    @Test
    @Transactional
    public void shouldSellFreeApartment(){
        Long newOwnerId = savedCustomers.get(6).getId();
        Long apartmentToSellId = savedApartments.get(18).getId();

        CustomerTO savedCustomer = sut.sellApartment(apartmentToSellId,newOwnerId);

        assertThat(savedCustomer.getApartments().stream()
                        .anyMatch(apartment -> apartment.getId().equals(apartmentToSellId)),
                is(equalTo(true)));
        assertThat(apartmentRepository.findById(apartmentToSellId).get().getStatus(), is(equalTo(ApartmentStatus.SOLD)));
        assertThat(customerRepository.findByApartment(apartmentToSellId).size(), is(equalTo(1)));
    }

    @Test
    @Transactional
    public void shouldSellReservedApartment(){
        Long newOwnerId = savedCustomers.get(11).getId();
        Long apartmentToSellId = savedApartments.get(1).getId();

        CustomerTO savedCustomer = sut.sellApartment(apartmentToSellId,newOwnerId);

        assertThat(savedCustomer.getApartments().stream()
                        .anyMatch(apartment -> apartment.getId().equals(apartmentToSellId)),
                is(equalTo(true)));
        assertThat(apartmentRepository.findById(apartmentToSellId).get().getStatus(), is(equalTo(ApartmentStatus.SOLD)));
        assertThat(customerRepository.findById(savedCustomers.get(0).getId()).get().getApartments()
                .stream().noneMatch(apartment -> apartment.getId().equals(apartmentToSellId)), is(equalTo(true)));
        assertThat(customerRepository.findById(savedCustomers.get(1).getId()).get().getApartments()
                .stream().noneMatch(apartment -> apartment.getId().equals(apartmentToSellId)), is(equalTo(true)));
        assertThat(customerRepository.findByApartment(apartmentToSellId).size(), is(equalTo(1)));
    }

    @Test
    @Transactional
    public void shouldSellSoldApartment(){
        Long newOwnerId = savedCustomers.get(11).getId();
        Long apartmentToSellId = savedApartments.get(0).getId();

        CustomerTO savedCustomer = sut.sellApartment(apartmentToSellId,newOwnerId);

        assertThat(savedCustomer.getApartments().stream()
                        .anyMatch(apartment -> apartment.getId().equals(apartmentToSellId)),
                is(equalTo(true)));
        assertThat(apartmentRepository.findById(apartmentToSellId).get().getStatus(), is(equalTo(ApartmentStatus.SOLD)));
        assertThat(customerRepository.findById(savedCustomers.get(0).getId()).get().getApartments()
                .stream().anyMatch(apartment -> apartment.getId().equals(apartmentToSellId)), is(equalTo(true)));
        assertThat(customerRepository.findByApartment(apartmentToSellId).size(), is(equalTo(2)));
    }

    @Test(expected = CustomerIsAlreadyOwnerOfThatApartmentException.class)
    @Transactional
    public void shouldExceptionWhenBuyAlreadyOwningApartment(){
        Long newOwnerId = savedCustomers.get(0).getId();
        Long apartmentToSellId = savedApartments.get(0).getId();

        sut.sellApartment(apartmentToSellId,newOwnerId);
    }

    @Test(expected = CustomerNotIncludedInApartmentReservationsException.class)
    @Transactional
    public void shouldExceptionWhenSellReservedApartmentToNotReservingCustomer(){
        Long newOwnerId = savedCustomers.get(10).getId();
        Long apartmentToSellId = savedApartments.get(1).getId();

        sut.sellApartment(apartmentToSellId,newOwnerId);
    }

    @Test
    @Transactional
    public void shouldCalculateSumCostOfSoldApartmentsByCustomer(){
        Long customerId = savedCustomers.stream()
                .filter(customerEnt -> customerEnt.getFirstName().equals("G"))
                .findFirst().get().getId();

        int sumCost = sut.calculateSumCostOfSoldApartmentsByCustomer(customerId);

        assertThat(sumCost, is(equalTo(60)));
    }

    @Test
    @Transactional
    public void shouldFindCustomersThatBoughtMoreThanOneApartment(){
        List<CustomerTO> foundCustomers = sut.findCustomersThatBoughtMoreThanOneApartment();

        assertThat(foundCustomers.size(), is(equalTo(3)));
        assertThat(foundCustomers.stream().anyMatch(cus -> cus.getFirstName().equals("G")),
                is(equalTo(true)));
        assertThat(foundCustomers.stream().anyMatch(cus -> cus.getFirstName().equals("H")),
                is(equalTo(true)));
        assertThat(foundCustomers.stream().anyMatch(cus -> cus.getFirstName().equals("I")),
                is(equalTo(true)));
    }

    @Transactional
    @Test(expected = OptimisticLockingFailureException.class)
    public void optimisticLockingTest(){
        CustomerTO newCustomer = new CustomerTO.CustomerBuilder().withFirstName("Błażej").withLastName("Cieśla").withEmail("email").build();
        CustomerEnt savedCustomer = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(newCustomer));
        CustomerEnt savedCustomer2 = customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(newCustomer));
        sut.sellApartment(savedApartments.get(0).getId(), savedCustomer.getId());
        sut.sellApartment(savedApartments.get(8).getId(), savedCustomer.getId());
    }

    @Test
    @Transactional
    public void noOptimisticLockExceptionWhenRead(){
        sut.findCustomersByApartment(savedApartments.get(0).getId());
        sut.findCustomersByApartment(savedApartments.get(1).getId());
    }

    @Transactional
    @Test
    public void removeCustomersNotRemoveApartments(){
        Long countBeforeDelete = apartmentRepository.count();

        customerRepository.deleteAll();

        assertThat(apartmentRepository.count(),is(equalTo(countBeforeDelete)));
    }
}
