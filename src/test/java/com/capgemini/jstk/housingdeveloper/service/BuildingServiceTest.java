package com.capgemini.jstk.housingdeveloper.service;

import com.capgemini.jstk.housingdeveloper.commons.ApartmentStatus;
import com.capgemini.jstk.housingdeveloper.entity.ApartmentEnt;
import com.capgemini.jstk.housingdeveloper.entity.BuildingEnt;
import com.capgemini.jstk.housingdeveloper.mappers.ApartmentMapper;
import com.capgemini.jstk.housingdeveloper.types.ApartmentTO;
import com.capgemini.jstk.housingdeveloper.types.BuildingTO;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class BuildingServiceTest extends DataBaseInserts{

    @Autowired
    private BuildingService sut;

    private List<BuildingEnt> savedBuildings;

    @Before
    public void insertToDataBase(){
        savedBuildings = buildAndSaveBuildings(buildApartments());
    }

    @Test
    @Transactional
    public void shouldCalculateAvgCostOfApartmentInBuilding() {
        Long idOfBuilding = savedBuildings.stream()
                .filter(buildingEnt -> buildingEnt.getLocalization().equals("Warszawa"))
                .findFirst().get().getId();

        int avg = sut.calculateAvgApartmentsCostByBuilding(idOfBuilding);

        assertThat(avg , is(equalTo(30)));
    }

    @Test
    @Transactional
    public void shouldCountNumberOfApartmentsByBuildingAndStatus(){
        Long idOfBuilding = savedBuildings.stream()
                .filter(buildingEnt -> buildingEnt.getLocalization().equals("Kraków"))
                .findFirst().get().getId();

        int count = sut.countNumberOfApartmentsByBuildingAndStatus(idOfBuilding, ApartmentStatus.FREE);

        assertThat(count, is(equalTo(2)));
    }

    @Test
    @Transactional
    public void shouldFindBuildingsWithMaxNumberOfFreeApartments(){
        List<BuildingTO> foundBuildings = sut.findBuildingsWithMaxNumberOfFreeApartments();

        assertThat(foundBuildings.size(),is(equalTo(2)));
        assertThat(foundBuildings.get(0).getLocalization(),is(isIn(Lists.newArrayList("Wrocław", "Kraków"))));
        assertThat(foundBuildings.get(1).getLocalization(),is(isIn(Lists.newArrayList("Wrocław", "Kraków"))));
        assertThat(foundBuildings.get(0).getLocalization(), is(not(equalTo(foundBuildings.get(1).getLocalization()))));
    }

    @Test
    @Transactional
    public void shouldCascadeRemoveApartmentFromBuilding(){
        BuildingEnt building = savedBuildings.stream()
                .filter(buildingEnt -> buildingEnt.getLocalization().equals("Kraków"))
                .findFirst().get();
        Set<ApartmentEnt> apartments = building.getApartments();
        apartments.add(ApartmentMapper.INSTANCE.toApartmentEnt(
                new ApartmentTO.ApartmentBuilder().withAddress("Bajana7").build()));
        buildingRepository.save(building);
        int beforeDelete = buildingRepository.findById(building.getId()).get().getApartments().size();

        Iterable<ApartmentEnt> savedApartmentsIterable = apartmentRepository.findAll();
        List<ApartmentEnt> savedApartments = Lists.newArrayList(savedApartmentsIterable);
        savedApartments.sort(Comparator.comparing(ApartmentEnt::getAddress));
        ApartmentEnt apartmentToDelete = savedApartments.get(14);


        apartmentRepository.delete(apartmentToDelete);


        int afterDelete = buildingRepository.findById(building.getId()).get().getApartments().size();

        assertThat(apartmentRepository.findById(apartmentToDelete.getId()).isPresent(), is(equalTo(false)));
        assertThat(beforeDelete -1, is(equalTo(afterDelete)));
    }

    @Transactional
    @Test
    public void shouldRemoveCascadeApartmentsWithRemoveBuilding(){
        BuildingEnt buildingEnt = buildingRepository.findById(savedBuildings.get(0).getId()).get();
        buildingRepository.delete(buildingEnt);

        assertThat(apartmentRepository.count(), is(equalTo(11L)));
    }


}