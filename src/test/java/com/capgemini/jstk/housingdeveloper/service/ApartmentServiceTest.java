package com.capgemini.jstk.housingdeveloper.service;

import com.capgemini.jstk.housingdeveloper.commons.ApartmentStatus;
import com.capgemini.jstk.housingdeveloper.entity.ApartmentEnt;
import com.capgemini.jstk.housingdeveloper.entity.CustomerEnt;
import com.capgemini.jstk.housingdeveloper.types.ApartmentTO;
import com.capgemini.jstk.housingdeveloper.types.NotSoldApartmentSearchCriteria;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class ApartmentServiceTest extends DataBaseInserts{

    @Autowired
    private ApartmentService sut;

    private List<ApartmentEnt> savedApartments;

    @Before
    public void insertToDataBase()  {
        buildAndSaveBuildings(buildApartments());
        Iterable<ApartmentEnt> savedApartmentsIterable = apartmentRepository.findAll();
        savedApartments = Lists.newArrayList(savedApartmentsIterable);
        savedApartments.sort(Comparator.comparing(ApartmentEnt::getAddress));
    }

    @Transactional
    @Test
    public void apartmentFoundByIdHasPassedStatus() {
        Long apartmentID = savedApartments.get(0).getId();

        boolean isSold = sut.apartmentByIdHasPassedStatus(apartmentID, ApartmentStatus.SOLD);

        assertThat(isSold,is(equalTo(true)));
    }

    @Transactional
    @Test
    public void shouldFindApartmentsAdaptedForDisables(){
        Set<ApartmentTO> apartmentsForDisabled = sut.findApartmentsAdaptedForDisabled();

        assertThat(apartmentsForDisabled.size(), is(equalTo(13)));
    }

    @Transactional
    @Test
    public void searchApartmentsBySearchCriteria(){
        NotSoldApartmentSearchCriteria searchCriteria = new NotSoldApartmentSearchCriteria.SearchCriteriaBuilder()
                .withMinSizeM2(50).build();

        Set<ApartmentTO> filteredApartments = sut.findNotSoldApartmentsBySearchCriteria(searchCriteria);

        assertThat(filteredApartments.size(), is(equalTo(3)));
    }

    @Transactional
    @Test
    public void searchAllApartmentsByNullSearchCriteria(){
        Long numberOfSavedApartments = apartmentRepository.count();
        NotSoldApartmentSearchCriteria searchCriteria = new NotSoldApartmentSearchCriteria.SearchCriteriaBuilder()
                .build();

        Set<ApartmentTO> filteredApartments = sut.findNotSoldApartmentsBySearchCriteria(searchCriteria);

        assertThat(filteredApartments.size(), is(equalTo(numberOfSavedApartments.intValue())));
    }


    @Test
    public void removeApartmentsRemovesUsersApartments(){
        List<CustomerEnt> actualCustomers = (List<CustomerEnt>) customerRepository.findAll();

        savedApartments.stream().forEach(apartment -> sut.deleteById(apartment.getId()));

        assertThat(apartmentRepository.count(),is(equalTo(0L)));
        assertThat(actualCustomers.stream().allMatch(customer -> customer.getApartments().isEmpty() == true),is(equalTo(true)));
    }
}
