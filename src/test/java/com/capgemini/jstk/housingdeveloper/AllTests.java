package com.capgemini.jstk.housingdeveloper;

import com.capgemini.jstk.housingdeveloper.repository.RepositoryTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.capgemini.jstk.housingdeveloper.service.ServiceTestSuite;

@RunWith(Suite.class)
@SuiteClasses({ServiceTestSuite.class, RepositoryTestSuite.class})
public class AllTests {

}