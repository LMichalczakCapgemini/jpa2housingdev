package com.capgemini.jstk.housingdeveloper.repository;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ApartmentRepositoryTest.class, BuildingRepositoryTest.class })
public class RepositoryTestSuite {
}
