package com.capgemini.jstk.housingdeveloper.repository;

import com.capgemini.jstk.housingdeveloper.entity.BuildingEnt;
import com.capgemini.jstk.housingdeveloper.mappers.BuildingMapper;
import com.capgemini.jstk.housingdeveloper.types.BuildingTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BuildingRepositoryTest {

    @Autowired
    private BuildingRepository sut;

    @Test(expected = OptimisticLockingFailureException.class)
    public void optimisticLockingException(){
        BuildingTO buildingTO = new BuildingTO.BuildingBuilder().build();
        BuildingEnt buildingEnt = sut.save(BuildingMapper.INSTANCE.toBuildingEnt(buildingTO));
        buildingEnt.setDescription("description");

        sut.save(buildingEnt);
        sut.save(buildingEnt);
    }

    @Test
    public void noOptimisticLockExceptionWhenRead(){
        sut.findAll();
        sut.findAll();
    }
}