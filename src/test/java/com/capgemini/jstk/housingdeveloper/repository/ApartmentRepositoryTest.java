package com.capgemini.jstk.housingdeveloper.repository;

import com.capgemini.jstk.housingdeveloper.entity.ApartmentEnt;
import com.capgemini.jstk.housingdeveloper.mappers.ApartmentMapper;
import com.capgemini.jstk.housingdeveloper.types.ApartmentTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApartmentRepositoryTest {

    @Autowired
    private ApartmentRepository sut;

    @Test(expected = OptimisticLockingFailureException.class)
    public void optimisticLockException(){
        ApartmentTO apartmentTO = new ApartmentTO.ApartmentBuilder().build();
        ApartmentEnt apartmentEnt = sut.save(ApartmentMapper.INSTANCE.toApartmentEnt(apartmentTO));
        apartmentEnt.setNumberOfRooms(10);

        sut.save(apartmentEnt);
        sut.save(apartmentEnt);
    }

    @Test
    public void noOptimisticLockExceptionWhenRead(){
        sut.findAll();
        sut.findAll();
    }
}