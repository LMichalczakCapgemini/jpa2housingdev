package com.capgemini.jstk.housingdeveloper.types;

import java.util.HashSet;
import java.util.Set;

public class BuildingTO extends AbstractTO{
    private Set<ApartmentTO> apartments;
    private String description;
    private String localization;
    private int numberOfFloors;
    private Boolean isElevator;

    public BuildingTO() {
    }

    public BuildingTO(BuildingBuilder builder){
        this.apartments = builder.apartments;
        this.description = builder.description;
        this.localization = builder.localization;
        this.numberOfFloors = builder.numberOfFloors;
        this.isElevator = builder.isElevator;
    }

    public Set<ApartmentTO> getApartments() {
        return apartments;
    }

    public void setApartments(Set<ApartmentTO> apartments) {
        this.apartments = apartments;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    public void setNumberOfFloors(int numberOfFloors) {
        this.numberOfFloors = numberOfFloors;
    }

    public Boolean getElevator() {
        return isElevator;
    }

    public void setElevator(Boolean elevator) {
        isElevator = elevator;
    }

    public static class BuildingBuilder {
        private Set<ApartmentTO> apartments = new HashSet<>();
        private String description;
        private String localization;
        private int numberOfFloors;
        private Boolean isElevator;

        public BuildingBuilder withApartment(ApartmentTO apartmentTO){
            this.apartments.add(apartmentTO);
            return this;
        }

        public BuildingBuilder withApartments(Set<ApartmentTO> apartmentTOs){
            this.apartments.addAll(apartmentTOs);
            return this;
        }

        public BuildingBuilder withDescription(String description){
            this.description = description;
            return this;
        }

        public BuildingBuilder withLocalization(String localization){
            this.localization = localization;
            return this;
        }

        public BuildingBuilder withNumberOfFloors(int numberOfFloors){
            this.numberOfFloors = numberOfFloors;
            return this;
        }

        public BuildingBuilder withIsElevator(boolean isElevator){
            this.isElevator = isElevator;
            return this;
        }

        public BuildingTO build() {
            return new BuildingTO(this);
        }
    }

}
