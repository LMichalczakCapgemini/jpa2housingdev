package com.capgemini.jstk.housingdeveloper.types;

import com.capgemini.jstk.housingdeveloper.commons.ApartmentStatus;

import java.util.Objects;

public class ApartmentTO extends AbstractTO {
    private int sizeM2;
    private int numberOfRooms;
    private int numberOfBalconies;
    private int floorNumber;
    private String address;
    private ApartmentStatus status;
    private int pricePln;

    public ApartmentTO() {
    }

    public ApartmentTO(ApartmentBuilder builder) {
        this.sizeM2 = builder.sizeM2;
        this.numberOfRooms = builder.numberOfRooms;
        this.numberOfBalconies = builder.numberOfBalconies;
        this.floorNumber = builder.floorNumber;
        this.address = builder.address;
        this.status = builder.status;
        this.pricePln = builder.pricePln;
    }

    public int getSizeM2() {
        return sizeM2;
    }

    public void setSizeM2(int sizeM2) {
        this.sizeM2 = sizeM2;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public int getNumberOfBalconies() {
        return numberOfBalconies;
    }

    public void setNumberOfBalconies(int numberOfBalconies) {
        this.numberOfBalconies = numberOfBalconies;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(int floorNumber) {
        this.floorNumber = floorNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ApartmentStatus getStatus() {
        return status;
    }

    public void setStatus(ApartmentStatus status) {
        this.status = status;
    }

    public int getPricePln() {
        return pricePln;
    }

    public void setPricePln(int pricePln) {
        this.pricePln = pricePln;
    }

    public static class ApartmentBuilder {
        private int sizeM2;
        private int numberOfRooms;
        private int numberOfBalconies;
        private int floorNumber;
        private String address;
        private ApartmentStatus status = ApartmentStatus.FREE;
        private int pricePln;

        public ApartmentBuilder withSizeM2(int sizeM2){
            this.sizeM2 = sizeM2;
            return this;
        }

        public ApartmentBuilder withNumberOfRooms(int numberOfRooms){
            this.numberOfRooms = numberOfRooms;
            return this;
        }

        public ApartmentBuilder withNumberOfBalconies(int numberOfBalconies){
            this.numberOfBalconies = numberOfBalconies;
            return this;
        }

        public ApartmentBuilder withFloorNumber(int floorNumber){
            this.floorNumber = floorNumber;
            return this;
        }

        public ApartmentBuilder withAddress(String address){
            this.address = address;
            return this;
        }

        public ApartmentBuilder withStatus(ApartmentStatus status){
            this.status = status;
            return this;
        }

        public ApartmentBuilder withPricePln(int pricePln){
            this.pricePln = pricePln;
            return this;
        }

        public ApartmentTO build(){
            return new ApartmentTO(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ApartmentTO)) return false;
        ApartmentTO that = (ApartmentTO) o;
        return sizeM2 == that.sizeM2 &&
                numberOfRooms == that.numberOfRooms &&
                numberOfBalconies == that.numberOfBalconies &&
                floorNumber == that.floorNumber &&
                pricePln == that.pricePln &&
                Objects.equals(address, that.address) &&
                status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sizeM2, numberOfRooms, numberOfBalconies, floorNumber, address, status, pricePln);
    }

    @Override
    public String toString() {
        return "ApartmentTO{" +
                "sizeM2=" + sizeM2 +
                ", numberOfRooms=" + numberOfRooms +
                ", numberOfBalconies=" + numberOfBalconies +
                ", floorNumber=" + floorNumber +
                ", address='" + address + '\'' +
                ", status=" + status +
                ", pricePln=" + pricePln +
                ", id=" + id +
                '}';
    }
}
