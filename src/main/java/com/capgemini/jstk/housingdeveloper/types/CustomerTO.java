package com.capgemini.jstk.housingdeveloper.types;

import java.util.HashSet;
import java.util.Set;

public class CustomerTO extends AbstractTO{
    private Set<ApartmentTO> apartments;
    private String firstName;
    private String lastName;
    private String email;

    public CustomerTO() {
    }

    public CustomerTO(CustomerBuilder builder) {
        this.apartments = builder.apartments;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.email = builder.email;
    }

    public Set<ApartmentTO> getApartments() {
        return apartments;
    }

    public void setApartments(Set<ApartmentTO> apartments) {
        this.apartments = apartments;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public static class CustomerBuilder {
        private Set<ApartmentTO> apartments = new HashSet<>();
        private String firstName;
        private String lastName;
        private String email;

        public CustomerBuilder withApartment(ApartmentTO apartmentTO){
            this.apartments.add(apartmentTO);
            return this;
        }

        public CustomerBuilder withApartments(Set<ApartmentTO> apartmentTOs){
            this.apartments.addAll(apartmentTOs);
            return this;
        }

        public CustomerBuilder withFirstName(String firstName){
            this.firstName = firstName;
            return this;
        }

        public CustomerBuilder withLastName(String lastName){
            this.lastName = lastName;
            return this;
        }

        public CustomerBuilder withEmail(String email){
            this.email = email;
            return this;
        }

        public CustomerTO build(){
            return new CustomerTO(this);
        }
    }
}
