package com.capgemini.jstk.housingdeveloper.types;

public class NotSoldApartmentSearchCriteria {
    private Integer minSizeM2;
    private Integer maxSizeM2;
    private Integer minNumberOfRooms;
    private Integer maxNumberOfRooms;
    private Integer minNumberOfBalconies;
    private Integer maxNumberOfBalconies;

    public NotSoldApartmentSearchCriteria() {
    }

    public NotSoldApartmentSearchCriteria(SearchCriteriaBuilder builder) {
        this.minSizeM2 = builder.minSizeM2;
        this.maxSizeM2 = builder.maxSizeM2;
        this.minNumberOfRooms = builder.minNumberOfRooms;
        this.maxNumberOfRooms = builder.maxNumberOfRooms;
        this.minNumberOfBalconies = builder.minNumberOfBalconies;
        this.maxNumberOfBalconies = builder.maxNumberOfBalconies;
    }

    public Integer getMinSizeM2() {
        return minSizeM2;
    }

    public Integer getMaxSizeM2() {
        return maxSizeM2;
    }

    public Integer getMinNumberOfRooms() {
        return minNumberOfRooms;
    }

    public Integer getMaxNumberOfRooms() {
        return maxNumberOfRooms;
    }

    public Integer getMinNumberOfBalconies() {
        return minNumberOfBalconies;
    }

    public Integer getMaxNumberOfBalconies() {
        return maxNumberOfBalconies;
    }

    public void setMinSizeM2(int minSizeM2) {
        this.minSizeM2 = minSizeM2;
    }

    public void setMaxSizeM2(int maxSizeM2) {
        this.maxSizeM2 = maxSizeM2;
    }

    public void setMinNumberOfRooms(int minNumberOfRooms) {
        this.minNumberOfRooms = minNumberOfRooms;
    }

    public void setMaxNumberOfRooms(int maxNumberOfRooms) {
        this.maxNumberOfRooms = maxNumberOfRooms;
    }

    public void setMinNumberOfBalconies(int minNumberOfBalconies) {
        this.minNumberOfBalconies = minNumberOfBalconies;
    }

    public void setMaxNumberOfBalconies(int maxNumberOfBalconies) {
        this.maxNumberOfBalconies = maxNumberOfBalconies;
    }

    public static class SearchCriteriaBuilder {
        private Integer minSizeM2;
        private Integer maxSizeM2;
        private Integer minNumberOfRooms;
        private Integer maxNumberOfRooms;
        private Integer minNumberOfBalconies;
        private Integer maxNumberOfBalconies;

        public SearchCriteriaBuilder withMinSizeM2(Integer minSizeM2){
            this.minSizeM2 = minSizeM2;
            return this;
        }

        public SearchCriteriaBuilder withMaxSizeM2(Integer maxSizeM2){
            this.maxSizeM2 = maxSizeM2;
            return this;
        }

        public SearchCriteriaBuilder withMinNumberOfRooms(Integer minNumberOfRooms){
            this.minNumberOfRooms = minNumberOfRooms;
            return this;
        }

        public SearchCriteriaBuilder withMaxNumberOfRooms(Integer maxNumberOfRooms){
            this.maxNumberOfRooms = maxNumberOfRooms;
            return this;
        }

        public SearchCriteriaBuilder withMinNumberOfBalconies(Integer minNumberOfBalconies){
            this.minNumberOfBalconies = minNumberOfBalconies;
            return this;
        }

        public SearchCriteriaBuilder withMaxNumberOfBalconies(Integer maxNumberOfBalconies){
            this.maxNumberOfBalconies = maxNumberOfBalconies;
            return this;
        }

        public NotSoldApartmentSearchCriteria build(){
            return new NotSoldApartmentSearchCriteria(this);
        }
    }
}
