package com.capgemini.jstk.housingdeveloper.service;

import com.capgemini.jstk.housingdeveloper.commons.ApartmentStatus;
import com.capgemini.jstk.housingdeveloper.types.ApartmentTO;
import com.capgemini.jstk.housingdeveloper.types.NotSoldApartmentSearchCriteria;

import java.util.Set;

public interface ApartmentService {
    ApartmentTO findById(Long id);

    Set<ApartmentTO> findNotSoldApartmentsBySearchCriteria(NotSoldApartmentSearchCriteria searchCriteria);

    Set<ApartmentTO> findApartmentsAdaptedForDisabled();

    boolean apartmentByIdHasPassedStatus(Long id, ApartmentStatus status);

    void deleteById(Long apartmentId);
}
