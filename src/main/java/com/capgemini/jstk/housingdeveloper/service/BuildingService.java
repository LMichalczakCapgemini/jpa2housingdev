package com.capgemini.jstk.housingdeveloper.service;

import com.capgemini.jstk.housingdeveloper.commons.ApartmentStatus;
import com.capgemini.jstk.housingdeveloper.types.BuildingTO;

import java.util.List;

public interface BuildingService {
    int calculateAvgApartmentsCostByBuilding(Long buildingId);

    int countNumberOfApartmentsByBuildingAndStatus(Long buildingId, ApartmentStatus apartmentStatus);

    List<BuildingTO> findBuildingsWithMaxNumberOfFreeApartments();
}
