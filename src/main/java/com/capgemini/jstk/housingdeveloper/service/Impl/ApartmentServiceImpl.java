package com.capgemini.jstk.housingdeveloper.service.Impl;

import com.capgemini.jstk.housingdeveloper.commons.ApartmentNotFoundException;
import com.capgemini.jstk.housingdeveloper.commons.ApartmentStatus;
import com.capgemini.jstk.housingdeveloper.repository.ApartmentRepository;
import com.capgemini.jstk.housingdeveloper.entity.ApartmentEnt;
import com.capgemini.jstk.housingdeveloper.mappers.ApartmentMapper;
import com.capgemini.jstk.housingdeveloper.service.ApartmentService;
import com.capgemini.jstk.housingdeveloper.service.CustomerService;
import com.capgemini.jstk.housingdeveloper.types.ApartmentTO;
import com.capgemini.jstk.housingdeveloper.types.CustomerTO;
import com.capgemini.jstk.housingdeveloper.types.NotSoldApartmentSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Transactional(readOnly = true)
@Service
public class ApartmentServiceImpl implements ApartmentService {

    @Autowired
    private ApartmentRepository apartmentRepository;

    @Autowired
    private CustomerService customerService;

    @Override
    public ApartmentTO findById(Long id) {
        Optional<ApartmentEnt> optionalApartmentEnt = apartmentRepository.findById(id);
        if(!optionalApartmentEnt.isPresent()){
            throw new ApartmentNotFoundException();
        }
        return ApartmentMapper.INSTANCE.toApartmentTO(optionalApartmentEnt.get());
    }

    @Override
    public Set<ApartmentTO> findNotSoldApartmentsBySearchCriteria(NotSoldApartmentSearchCriteria searchCriteria) {
        prepareSearchCriteria(searchCriteria);
        return ApartmentMapper.INSTANCE.map2TOs(
                ApartmentMapper.INSTANCE.mapEntsListToSet(
                        apartmentRepository.findNotSoldApartmentsBySearchCriteria(searchCriteria)));
    }

    private void prepareSearchCriteria(NotSoldApartmentSearchCriteria searchCriteria){
        if (searchCriteria.getMinSizeM2() == null){
            searchCriteria.setMinSizeM2(0);
        }
        if (searchCriteria.getMaxSizeM2() == null){
            searchCriteria.setMaxSizeM2(1000);
        }
        if (searchCriteria.getMinNumberOfBalconies() == null){
            searchCriteria.setMinNumberOfBalconies(0);
        }
        if(searchCriteria.getMaxNumberOfBalconies() == null){
            searchCriteria.setMaxNumberOfBalconies(10);
        }
        if(searchCriteria.getMinNumberOfRooms() == null){
            searchCriteria.setMinNumberOfRooms(0);
        }
        if (searchCriteria.getMaxNumberOfRooms() == null){
            searchCriteria.setMaxNumberOfRooms(20);
        }
    }

    @Override
    public Set<ApartmentTO> findApartmentsAdaptedForDisabled() {
        return ApartmentMapper.INSTANCE.map2TOs(
                ApartmentMapper.INSTANCE.mapEntsListToSet(
                        apartmentRepository.findApartmentsAdaptedForDisabled()));
    }

    @Override
    public boolean apartmentByIdHasPassedStatus(Long id, ApartmentStatus status) {
        return apartmentRepository.apartmentByIdHasPassedStatus(id,status);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteById(Long apartmentId) {
        ApartmentTO apartmentToRemove = findById(apartmentId);
        customerService.removeApartmentFromCustomersLists(apartmentToRemove);
        apartmentRepository.deleteById(apartmentId);
    }

}
