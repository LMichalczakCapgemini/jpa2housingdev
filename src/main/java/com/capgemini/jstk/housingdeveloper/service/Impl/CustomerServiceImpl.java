package com.capgemini.jstk.housingdeveloper.service.Impl;

import com.capgemini.jstk.housingdeveloper.commons.*;
import com.capgemini.jstk.housingdeveloper.repository.CustomerRepository;
import com.capgemini.jstk.housingdeveloper.entity.CustomerEnt;
import com.capgemini.jstk.housingdeveloper.mappers.CustomerMapper;
import com.capgemini.jstk.housingdeveloper.service.ApartmentService;
import com.capgemini.jstk.housingdeveloper.service.CustomerService;
import com.capgemini.jstk.housingdeveloper.types.ApartmentTO;
import com.capgemini.jstk.housingdeveloper.types.CustomerTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Transactional(readOnly = true)
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ApartmentService apartmentService;

    @Override
    public CustomerTO findById(Long id) {
        Optional<CustomerEnt> optionalCustomerEnt = customerRepository.findById(id);
        if (!optionalCustomerEnt.isPresent()){
            throw new CustomerNotFoundException();
        }
        return CustomerMapper.INSTANCE.toCustomerTO(optionalCustomerEnt.get());
    }

    @Override
    public List<CustomerTO> findCustomersByApartment(Long apartmentId) {
        return CustomerMapper.INSTANCE.map2TOs(customerRepository.findByApartment(apartmentId));
    }

    @Override
    @Transactional(readOnly = false)
    public CustomerTO reserveApartment(Long apartmentId, Long customerId) {
        exceptionIfReservationNotValid(apartmentId,customerId);

        ApartmentTO apartmentTO = apartmentService.findById(apartmentId);
        apartmentTO.setStatus(ApartmentStatus.RESERVED);

        CustomerTO newReservingCustomer = this.findById(customerId);
        addApartmentToCustomerList(apartmentTO, newReservingCustomer);

        return CustomerMapper.INSTANCE.toCustomerTO(
                customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(newReservingCustomer))
        );
    }

    @Override
    @Transactional(readOnly = false)
    public CustomerTO sellApartment(Long apartmentId, Long customerId) {
        exceptionIfSellNotValid(apartmentId, customerId);

        ApartmentTO apartmentToByu = apartmentService.findById(apartmentId);
        if(apartmentToByu.getStatus() != ApartmentStatus.SOLD){
            prepareApartmentToByu(apartmentToByu);
        }
        CustomerTO newOwnerCustomer = findById(customerId);
        addApartmentToCustomerList(apartmentToByu, newOwnerCustomer);

        return CustomerMapper.INSTANCE.toCustomerTO(
                customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(newOwnerCustomer))
        );
    }

    private void prepareApartmentToByu(ApartmentTO apartmentToByu) {
        if (apartmentToByu.getStatus() == ApartmentStatus.RESERVED){
            removeApartmentFromCustomersLists(apartmentToByu);
        }
        apartmentToByu.setStatus(ApartmentStatus.SOLD);
    }

    @Override
    @Transactional(readOnly = false)
    public void removeApartmentFromCustomersLists(ApartmentTO apartmentToByu) {
        List<CustomerTO> allReservingCustomers = CustomerMapper.INSTANCE.map2TOs(
                customerRepository.findByApartment(apartmentToByu.getId()));

        allReservingCustomers.stream()
                .forEach(customerTO -> {
                    deleteApartmentFromCustomerList(apartmentToByu, customerTO);
                    customerRepository.save(CustomerMapper.INSTANCE.toCustomerEnt(customerTO));
        });
    }

    private void addApartmentToCustomerList(ApartmentTO apartmentTO, CustomerTO customerTO){
        Set<ApartmentTO> customerApartments = customerTO.getApartments();
        customerApartments.add(apartmentTO);
        customerTO.setApartments(customerApartments);
    }

    private void deleteApartmentFromCustomerList(ApartmentTO apartmentTO, CustomerTO customerTO){
        Set<ApartmentTO> customerApartments = customerTO.getApartments();
        customerApartments.remove(apartmentTO);
        customerTO.setApartments(customerApartments);
    }

    @Override
    public int calculateSumCostOfSoldApartmentsByCustomer(Long customerId) {
        return customerRepository.calculateSumCostOfSoldApartmentsByCustomer(customerId);
    }

    @Override
    public List<CustomerTO> findCustomersThatBoughtMoreThanOneApartment() {
        return CustomerMapper.INSTANCE
                .map2TOs(customerRepository.findCustomersThatBoughtMoreThanOneApartment());
    }

    private void exceptionIfReservationNotValid(Long apartmentId, Long customerId){
        if(customerRepository.checkOwnershipOfTheApartmentByCustomer(apartmentId, customerId) == true) {
            throw new CustomerIsAlreadyOwnerOfThatApartmentException(apartmentId, customerId);
        }
        if(apartmentService.apartmentByIdHasPassedStatus(apartmentId, ApartmentStatus.SOLD)) {
            throw new ApartmentIsSoldException();
        }
        if(customerRepository.countApartmentsWhereCustomerIsOnlyBooking(customerId) > 2 &&
                !(customerRepository.countOtherCustomersThatReservedTheApartment(apartmentId, customerId) > 0)) {
            throw new CustomerMadeMaxNumberOfOnlyOwningReservationsException();
        }
    }

    private void exceptionIfSellNotValid(Long apartmentId, Long customerId){
        if(customerRepository.checkOwnershipOfTheApartmentByCustomer(apartmentId, customerId) == true &&
            apartmentService.apartmentByIdHasPassedStatus(apartmentId,ApartmentStatus.SOLD)) {
            throw new CustomerIsAlreadyOwnerOfThatApartmentException(apartmentId, customerId);
        }

        if(apartmentService.apartmentByIdHasPassedStatus(apartmentId, ApartmentStatus.RESERVED) == true &&
            customerRepository.findByApartment(apartmentId).stream()
                    .noneMatch(customerEnt -> customerEnt.getId().equals(customerId))){
            throw new CustomerNotIncludedInApartmentReservationsException();
        }
    }
}
