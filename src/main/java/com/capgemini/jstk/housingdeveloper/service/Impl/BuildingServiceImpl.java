package com.capgemini.jstk.housingdeveloper.service.Impl;

import com.capgemini.jstk.housingdeveloper.commons.ApartmentStatus;
import com.capgemini.jstk.housingdeveloper.mappers.BuildingMapper;
import com.capgemini.jstk.housingdeveloper.repository.BuildingRepository;
import com.capgemini.jstk.housingdeveloper.service.BuildingService;
import com.capgemini.jstk.housingdeveloper.types.BuildingTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class BuildingServiceImpl implements BuildingService {
    @Autowired
    private BuildingRepository buildingRepository;

    @Override
    public int calculateAvgApartmentsCostByBuilding(Long buildingId) {
        return buildingRepository.calculateAvgApartmentsCostByBuilding(buildingId);
    }

    @Override
    public int countNumberOfApartmentsByBuildingAndStatus(Long buildingId, ApartmentStatus apartmentStatus) {
        return buildingRepository.countNumberOfApartmentsByBuildingAndStatus(buildingId, apartmentStatus);
    }

    @Override
    public List<BuildingTO> findBuildingsWithMaxNumberOfFreeApartments() {
        return BuildingMapper.INSTANCE.map2TOs(buildingRepository.findBuildingsWithMaxNumberOfFreeApartments());
    }
}
