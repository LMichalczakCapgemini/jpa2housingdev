package com.capgemini.jstk.housingdeveloper.service;

import com.capgemini.jstk.housingdeveloper.types.ApartmentTO;
import com.capgemini.jstk.housingdeveloper.types.CustomerTO;

import java.util.List;

public interface CustomerService {
    CustomerTO findById(Long id);

    List<CustomerTO> findCustomersByApartment(Long apartmentId);

    CustomerTO reserveApartment(Long apartmentId, Long customerId);

    CustomerTO sellApartment(Long apartmentId, Long customerId);

    int calculateSumCostOfSoldApartmentsByCustomer(Long customerId);

    List<CustomerTO> findCustomersThatBoughtMoreThanOneApartment();

    void removeApartmentFromCustomersLists(ApartmentTO apartmentToRemove);
}
