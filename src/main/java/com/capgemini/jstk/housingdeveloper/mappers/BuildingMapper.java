package com.capgemini.jstk.housingdeveloper.mappers;

import com.capgemini.jstk.housingdeveloper.entity.BuildingEnt;
import com.capgemini.jstk.housingdeveloper.types.BuildingTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = ApartmentMapper.class)
public interface BuildingMapper {
    BuildingMapper INSTANCE = Mappers.getMapper(BuildingMapper.class);

    BuildingTO toBuildingTO(BuildingEnt buildingEnt);

    BuildingEnt toBuildingEnt(BuildingTO buildingTO);

    List<BuildingTO> map2TOs(List<BuildingEnt> buildingEnts);

    List<BuildingEnt> map2Ents(List<BuildingTO> buildingTOs);
}
