package com.capgemini.jstk.housingdeveloper.mappers;

import com.capgemini.jstk.housingdeveloper.entity.CustomerEnt;
import com.capgemini.jstk.housingdeveloper.types.CustomerTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = ApartmentMapper.class)
public interface CustomerMapper {
    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    CustomerTO toCustomerTO(CustomerEnt customerEnt);

    CustomerEnt toCustomerEnt(CustomerTO customerTO);

    List<CustomerTO> map2TOs(List<CustomerEnt> customerEnts);
}
