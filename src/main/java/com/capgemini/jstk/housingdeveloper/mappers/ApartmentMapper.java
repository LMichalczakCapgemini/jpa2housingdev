package com.capgemini.jstk.housingdeveloper.mappers;

import com.capgemini.jstk.housingdeveloper.entity.ApartmentEnt;
import com.capgemini.jstk.housingdeveloper.types.ApartmentTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;

@Mapper
public interface ApartmentMapper {
    ApartmentMapper INSTANCE = Mappers.getMapper(ApartmentMapper.class);
    
    ApartmentTO toApartmentTO(ApartmentEnt apartmentEnt);

    ApartmentEnt toApartmentEnt(ApartmentTO apartmentTO);

    Set<ApartmentTO> map2TOs(Set<ApartmentEnt> apartmentEnts);

    Set<ApartmentEnt> map2Ents(Set<ApartmentTO> apartmentTOs);

    Set<ApartmentEnt> mapEntsListToSet(List<ApartmentEnt> apartmentEntList);
}
