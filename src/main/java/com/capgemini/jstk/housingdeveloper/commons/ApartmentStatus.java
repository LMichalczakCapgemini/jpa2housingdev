package com.capgemini.jstk.housingdeveloper.commons;

public enum ApartmentStatus {
    FREE,
    RESERVED,
    SOLD
}
