package com.capgemini.jstk.housingdeveloper.commons;

public class CustomerIsAlreadyOwnerOfThatApartmentException extends RuntimeException {
    public String message;

    public CustomerIsAlreadyOwnerOfThatApartmentException(Long apartmentId, Long customerId){
        this.message = "Customer "+  customerId + " has already reserved or bought apartment "+ apartmentId;
    }
}
