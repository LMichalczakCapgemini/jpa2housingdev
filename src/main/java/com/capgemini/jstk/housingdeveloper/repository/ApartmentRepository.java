package com.capgemini.jstk.housingdeveloper.repository;

import com.capgemini.jstk.housingdeveloper.commons.ApartmentStatus;
import com.capgemini.jstk.housingdeveloper.entity.ApartmentEnt;
import com.capgemini.jstk.housingdeveloper.repository.custom.ApartmentRepositoryCustom;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ApartmentRepository extends CrudRepository<ApartmentEnt, Long>,
        ApartmentRepositoryCustom {
    @Query(" select apartment from BuildingEnt building join building.apartments apartment " +
            " where building.isElevator = true or apartment.floorNumber = 0")
    List<ApartmentEnt> findApartmentsAdaptedForDisabled();

    @Query("select case " +
            "when apartment.status = :status then true " +
            "else false end " +
            "from ApartmentEnt apartment where apartment.id = :apartmentId")
    boolean apartmentByIdHasPassedStatus(@Param("apartmentId") Long id, @Param("status") ApartmentStatus status);
}
