package com.capgemini.jstk.housingdeveloper.repository.custom;

import com.capgemini.jstk.housingdeveloper.entity.BuildingEnt;

import java.util.List;

public interface BuildingRepositoryCustom {
    List<BuildingEnt> findBuildingsWithMaxNumberOfFreeApartments();
}
