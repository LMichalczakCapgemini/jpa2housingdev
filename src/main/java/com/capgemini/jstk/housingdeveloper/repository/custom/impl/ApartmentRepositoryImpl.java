package com.capgemini.jstk.housingdeveloper.repository.custom.impl;

import com.capgemini.jstk.housingdeveloper.entity.ApartmentEnt;
import com.capgemini.jstk.housingdeveloper.entity.QApartmentEnt;
import com.capgemini.jstk.housingdeveloper.repository.custom.ApartmentRepositoryCustom;
import com.capgemini.jstk.housingdeveloper.types.NotSoldApartmentSearchCriteria;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ApartmentRepositoryImpl implements ApartmentRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ApartmentEnt> findNotSoldApartmentsBySearchCriteria(NotSoldApartmentSearchCriteria searchCriteria) {
        QApartmentEnt apartmentEnt = QApartmentEnt.apartmentEnt;
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);

        return queryFactory
                .selectFrom(apartmentEnt)
                .where(apartmentEnt.sizeM2.between(searchCriteria.getMinSizeM2(),searchCriteria.getMaxSizeM2())
                .and(apartmentEnt.numberOfRooms.between(searchCriteria.getMinNumberOfRooms(), searchCriteria.getMaxNumberOfRooms())
                .and(apartmentEnt.numberOfBalconies.between(searchCriteria.getMinNumberOfBalconies(), searchCriteria.getMaxNumberOfBalconies()))))
                .fetch();
    }
}
