package com.capgemini.jstk.housingdeveloper.repository.custom;

import com.capgemini.jstk.housingdeveloper.entity.ApartmentEnt;
import com.capgemini.jstk.housingdeveloper.types.NotSoldApartmentSearchCriteria;

import java.util.List;

public interface ApartmentRepositoryCustom {
    List<ApartmentEnt> findNotSoldApartmentsBySearchCriteria(NotSoldApartmentSearchCriteria searchCriteria);
}
