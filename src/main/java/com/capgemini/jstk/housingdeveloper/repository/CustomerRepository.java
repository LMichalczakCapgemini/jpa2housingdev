package com.capgemini.jstk.housingdeveloper.repository;

import com.capgemini.jstk.housingdeveloper.entity.CustomerEnt;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CustomerRepository extends CrudRepository<CustomerEnt, Long> {

    @Query("select customer from CustomerEnt customer join customer.apartments apartment " +
            "where apartment.id = ?1 and (apartment.status = 'RESERVED' or apartment.status = 'SOLD') ")
    List<CustomerEnt> findByApartment(Long apartmentId);

    @Query("select count(customer) from CustomerEnt customer join customer.apartments apartment " +
            " where customer.id = ?1 and apartment.id in (select apartment.id" +
            " from CustomerEnt customer join customer.apartments apartment " +
            " where apartment.status = 'RESERVED' " +
            " group by apartment " +
            " having count(customer) = 1)" )
    int countApartmentsWhereCustomerIsOnlyBooking(Long customerId);

    @Query("select case " +
            " when count(apartment) = 1 then true else false end  " +
            " from CustomerEnt customer join customer.apartments apartment " +
            " where customer.id = :customerId and apartment.Id = :apartmentId")
    boolean checkOwnershipOfTheApartmentByCustomer(
            @Param("apartmentId") Long apartmentId,
            @Param("customerId") Long customerId);
    
    @Query(" select count(apartment) from CustomerEnt customer join customer.apartments apartment " +
            " where customer.id <> :customerId and apartment.Id = :apartmentId and  apartment.status = 'RESERVED' ")
    int countOtherCustomersThatReservedTheApartment(
            @Param("apartmentId") Long apartmentId,
            @Param("customerId") Long customerId);

    @Query(" select sum(apartment.pricePln) from CustomerEnt customer join customer.apartments apartment " +
            " where customer.id = ?1 and apartment.status = 'SOLD'")
    int calculateSumCostOfSoldApartmentsByCustomer(Long customerId);

    @Query(" select customer from CustomerEnt customer join customer.apartments apartment " +
            " where apartment.status = 'SOLD' " +
            " group by customer having count(apartment) > 1")
    List<CustomerEnt> findCustomersThatBoughtMoreThanOneApartment();
}
