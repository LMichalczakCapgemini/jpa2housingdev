package com.capgemini.jstk.housingdeveloper.repository.custom.impl;

import com.capgemini.jstk.housingdeveloper.commons.ApartmentStatus;
import com.capgemini.jstk.housingdeveloper.entity.BuildingEnt;
import com.capgemini.jstk.housingdeveloper.entity.QApartmentEnt;
import com.capgemini.jstk.housingdeveloper.entity.QBuildingEnt;
import com.capgemini.jstk.housingdeveloper.repository.custom.BuildingRepositoryCustom;
import com.querydsl.jpa.impl.JPAQueryFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class BuildingRepositoryImpl implements BuildingRepositoryCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<BuildingEnt> findBuildingsWithMaxNumberOfFreeApartments() {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        QBuildingEnt building = QBuildingEnt.buildingEnt;
        QApartmentEnt apartment = new QApartmentEnt("apartment");

        return queryFactory
                .selectFrom(building).join(building.apartments, apartment)
                .where(apartment.status.eq(ApartmentStatus.FREE))
                .groupBy(building)
                .having(building.count().eq(
                        queryFactory
                                .select( building.count())
                                .from(building)
                                .join(building.apartments, apartment)
                                .where(apartment.status.eq(ApartmentStatus.FREE))
                                .groupBy(building)
                                .orderBy(building.count().desc())
                                .fetchFirst()))
                .fetch();
    }
}
