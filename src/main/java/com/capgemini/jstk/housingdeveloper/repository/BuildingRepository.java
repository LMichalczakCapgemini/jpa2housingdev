package com.capgemini.jstk.housingdeveloper.repository;

import com.capgemini.jstk.housingdeveloper.commons.ApartmentStatus;
import com.capgemini.jstk.housingdeveloper.entity.BuildingEnt;
import com.capgemini.jstk.housingdeveloper.repository.custom.BuildingRepositoryCustom;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface BuildingRepository extends CrudRepository<BuildingEnt, Long>,
        BuildingRepositoryCustom {
    @Query(" select avg(apartment.pricePln) from BuildingEnt building join building.apartments apartment " +
            "where building.id = ?1 ")
    int calculateAvgApartmentsCostByBuilding(Long buildingId);

    @Query("select count(apartment) from BuildingEnt building join building.apartments apartment " +
            " where building.id = :buildingId and apartment.status = :apartmentStatus ")
    int countNumberOfApartmentsByBuildingAndStatus(
            @Param("buildingId") Long buildingId, @Param("apartmentStatus") ApartmentStatus apartmentStatus);

}
