package com.capgemini.jstk.housingdeveloper.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "CUSTOMER")
public class CustomerEnt extends AbstractEnt {
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(
            name = "CUSTOMER_APARTMENT",
            joinColumns = {@JoinColumn(name = "customer_id")},
            inverseJoinColumns = {@JoinColumn(name = "apartment_id")}
    )
    private Set<ApartmentEnt> apartments = new HashSet<>();
    @Column(nullable = false, length = 50)
    private String firstName;
    @Column(nullable = false, length = 50)
    private String lastName;
    @Column(nullable = false, length = 100)
    private String email;

    public CustomerEnt() {
    }

    public Set<ApartmentEnt> getApartments() {
        return apartments;
    }

    public void setApartments(Set<ApartmentEnt> apartments) {
        this.apartments = apartments;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
