package com.capgemini.jstk.housingdeveloper.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class AbstractEnt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @Version
    public int version;

    //@CreationTimestamp
    protected Timestamp createDate;

    //@UpdateTimestamp
    protected Timestamp lastUpdateDate;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public void setLastUpdateDate(Timestamp lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    @PrePersist
    public void setCreateDate() {
        this.createDate = Timestamp.valueOf(LocalDateTime.now());
        setLastUpdateDate();
    }

    public Timestamp getLastUpdateDate() {
        return lastUpdateDate;
    }

    @PreUpdate
    public void setLastUpdateDate() {
        this.lastUpdateDate = Timestamp.valueOf(LocalDateTime.now());
    }
}
