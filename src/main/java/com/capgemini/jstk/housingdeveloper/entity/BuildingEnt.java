package com.capgemini.jstk.housingdeveloper.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "BUILDING")
public class BuildingEnt extends AbstractEnt{
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "building_id")
    private Set<ApartmentEnt> apartments = new HashSet<>();
    @Column(length = 3000)
    private String description;
    @Column(length = 30)
    private String localization;
    @Column
    private int numberOfFloors;
    @Column()
    private Boolean isElevator;

    public BuildingEnt() {
    }

    public Set<ApartmentEnt> getApartments() {
        return apartments;
    }

    public void setApartments(Set<ApartmentEnt> apartments) {
        this.apartments = apartments;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    public void setNumberOfFloors(int numberOfFloors) {
        this.numberOfFloors = numberOfFloors;
    }

    public Boolean getElevator() {
        return isElevator;
    }

    public void setElevator(Boolean elevator) {
        isElevator = elevator;
    }
}
