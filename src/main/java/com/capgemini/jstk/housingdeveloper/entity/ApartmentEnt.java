package com.capgemini.jstk.housingdeveloper.entity;

import com.capgemini.jstk.housingdeveloper.commons.ApartmentStatus;

import javax.persistence.*;

@Entity
@Table(name = "APARTMENT")
public class ApartmentEnt extends AbstractEnt {
    @Column(name = "size_m2")
    private int sizeM2;
    @Column
    private int numberOfRooms;
    @Column
    private int numberOfBalconies;
    @Column
    private int floorNumber;
    @Column(length = 150)
    private String address;
    @Enumerated(EnumType.STRING)
    private ApartmentStatus status;
    @Column
    private int pricePln;

    public ApartmentEnt() {
    }

    public int getSizeM2() {
        return sizeM2;
    }

    public void setSizeM2(int sizeM2) {
        this.sizeM2 = sizeM2;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public int getNumberOfBalconies() {
        return numberOfBalconies;
    }

    public void setNumberOfBalconies(int numberOfBalconies) {
        this.numberOfBalconies = numberOfBalconies;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(int floorNumber) {
        this.floorNumber = floorNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ApartmentStatus getStatus() {
        return status;
    }

    public void setStatus(ApartmentStatus status) {
        this.status = status;
    }

    public int getPricePln() {
        return pricePln;
    }

    public void setPricePln(int pricePln) {
        this.pricePln = pricePln;
    }
}
